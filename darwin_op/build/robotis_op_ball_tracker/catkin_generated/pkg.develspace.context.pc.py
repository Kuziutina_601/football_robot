# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/kuzyutina/darwin_op/devel/include;/home/kuzyutina/darwin_op/src/robotis_op_ball_tracker/include".split(';') if "/home/kuzyutina/darwin_op/devel/include;/home/kuzyutina/darwin_op/src/robotis_op_ball_tracker/include" != "" else []
PROJECT_CATKIN_DEPENDS = "cv_bridge;joy;nav_msgs;roscpp;rospy;sensor_msgs;std_msgs;tf;dynamic_reconfigure".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrobotis_op_ball_tracker".split(';') if "-lrobotis_op_ball_tracker" != "" else []
PROJECT_NAME = "robotis_op_ball_tracker"
PROJECT_SPACE_DIR = "/home/kuzyutina/darwin_op/devel"
PROJECT_VERSION = "0.0.0"
