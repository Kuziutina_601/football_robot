#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/kuzyutina/darwin_op/src/darwin_gazebo"

# snsure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/kuzyutina/darwin_op/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/kuzyutina/darwin_op/install/lib/python2.7/dist-packages:/home/kuzyutina/darwin_op/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/kuzyutina/darwin_op/build" \
    "/usr/bin/python" \
    "/home/kuzyutina/darwin_op/src/darwin_gazebo/setup.py" \
    build --build-base "/home/kuzyutina/darwin_op/build/darwin_gazebo" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/kuzyutina/darwin_op/install" --install-scripts="/home/kuzyutina/darwin_op/install/bin"
