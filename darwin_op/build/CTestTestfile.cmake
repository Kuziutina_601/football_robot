# CMake generated Testfile for 
# Source directory: /home/kuzyutina/darwin_op/src
# Build directory: /home/kuzyutina/darwin_op/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(darwin_control)
SUBDIRS(darwin_description)
SUBDIRS(robotis_op_ball_tracker)
SUBDIRS(robotis_op_teleop)
SUBDIRS(darwin_gazebo)
