
#include <robotis_op_ball_tracker/robotis_op_ball_tracker_node.h>

using namespace robotis_op;

#include <iostream>

#include <stdlib.h>     /* srand, rand */
#include <std_msgs/Float64.h>
#include <math.h>
#include <sensor_msgs/image_encodings.h>

#include <image_transport/image_transport.h>
namespace robotis_op {


RobotisOPBallTrackingNode::RobotisOPBallTrackingNode(ros::NodeHandle nh)
    : nh_(nh)
{
    ros::NodeHandle nh_;

    joint_states_sub_ = nh_.subscribe("/darwin/joint_states", 100, &RobotisOPBallTrackingNode::jointStatesCb, this);
    image_sub_ = nh_.subscribe("/darwin/camera/image_raw", 1, &RobotisOPBallTrackingNode::imageCb, this);

    transformed_img_pub_ =nh_.advertise<sensor_msgs::Image>("/darwin/ball_tracker/image_transformed_raw", 100);
    blob_img_pub_ =nh_.advertise<sensor_msgs::Image>("/darwin/ball_tracker/image_blob_raw", 100);
    tilt_pub_ = nh_.advertise<std_msgs::Float64>("/darwin/j_tilt_position_controller/command", 100);
    pan_pub_ = nh_.advertise<std_msgs::Float64>("/darwin/j_pan_position_controller/command", 100);
    vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/darwin/cmd_vel", 1);
    count_no_detection_ = 0;
    count_search_loop_ = 0;
	go_count=0;
	direction = 0;
    gone = false;
	detected = false;
	goal_found = false;
/*
    dp_ = 1.0;
    minDist_ = 1.0;
    param1_ = 200;
    param2_ = 10;
    min_radius_ = 0.0;
    max_radius_ = 0.0;*/
}

RobotisOPBallTrackingNode::~RobotisOPBallTrackingNode()
{
}


void RobotisOPBallTrackingNode::jointStatesCb(const sensor_msgs::JointState& msg)
{
    pan_ = msg.position.at(10);
    tilt_ = msg.position.at(21);
}

void RobotisOPBallTrackingNode::dynamicReconfigureCb(robotis_op_ball_tracker::robotis_op_ball_trackerConfig &config, uint32_t level)
{

    dp_ = config.dp;
    minDist_ = config.minDist;
    param1_ = config.param1;
    param2_ = config.param2;
    min_radius_ = config.minRadius;
    max_radius_ = config.maxRadius;
}


static double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}


bool RobotisOPBallTrackingNode::findGoalFunc(cv::Mat& mat_trans, cv::Point& offset, cv::Point& width) {
	
	ROS_INFO("start detect goal");
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(mat_trans,contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    std::vector<cv::Point> approx;

	int maxX = -1, minX = 5000, sr;
    for (int i = 0; i < contours.size(); i++)
    {
		cv::Point p1(-1,-1), p2(-1,5000), p3(5000, 5000), p4(5000,-1);
	    cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);
		//ROS_INFO("approx size %d", approx.size());
        if (approx.size() >= 6 && approx.size() <= 12)
	    {
			 //ROS_INFO("image^^ %i %i", mat_trans.cols, mat_trans.rows);
             //ROS_INFO("I FIND %d %d kolvo %i", approx[0].x, approx[0].y, approx.size());
             for (int k = 0; k < approx.size(); k++){
				if(approx[k].x > maxX) maxX = approx[k].x;
				if(approx[k].x < minX) minX = approx[k].x;
                // ROS_INFO("%i  ==  %d,  %d", k, approx[k].x, approx[k].y);
             }
   			//goal_detected = true;
			/*image_transport::ImageTransport it(nh_);
			image_transport::Publisher pub = it.advertise("/own/goal_detect2/camera/image_raw", 1000);
			sensor_msgs::ImagePtr msg2 = cv_bridge::CvImage(std_msgs::Header(), "mono8", mat_trans).toImageMsg();
			pub.publish(msg2);*/
			sr = (maxX-minX)/2.0 + minX;
			//sr = mat_trans.cols / 2;
			int hi = 0;
			for (int k =0; k < approx.size(); k++) {
				if (approx[k].x < sr) {
					if(approx[k].y > p1.y) p1 = approx[k];
					if(approx[k].y < p2.y) p2 = approx[k];
				}
				else{
				    if(approx[k].y > p4.y) p4 = approx[k];
					if(approx[k].y < p3.y) p3 = approx[k];
				}		
				if (mat_trans.size().height/2.0 >= approx[k].y) hi++;	
				 
			}

			bool allInit = (p1.x == -1 && p1.y == -1) || (p2.x == -1 && p2.y == 5000) || (p3.x == 5000 && p3.y == 5000) || (p4.x == 5000 && p4.y == -1);
			double x_cen = mat_trans.size().width/2.0;
			bool full = p1.x < x_cen && p4.x > x_cen;
			width.x = p3.x-p1.x;
            double angle1 = angle(p1, p3, p2);
            double angle2 = angle(p2, p4, p3);
			
			ROS_INFO("angles %f", std::fabs(angle1 + angle2) );
          //  ROS_INFO("angles %d gg", angle1);
			if(std::fabs(angle1 + angle2) < 0.4 && hi >= 4 && !allInit && full) {
			    //goal_detected = true;
                
				ROS_INFO("I FIND");
				cv::Point goal_position(sr, mat_trans.size().height/2.0);
				cv::Point image_center(mat_trans.size().width/2.0,mat_trans.size().height/2.0);
		        offset = goal_position - image_center ;
				return true;
            }
		
	    }
	
    }
	return false;
}


void RobotisOPBallTrackingNode::walkWithBall(cv::Mat& mat_trans) {
	

	if (go_count < 20){

	ROS_INFO("start walking");
    geometry_msgs::Twist vel;
    vel.angular.z = 0.01;
    vel.linear.x = 0.3;
    vel.linear.y = 0;
    vel_pub_.publish(vel);
	
	go_count++;
	}
	else {
		//head movement

		ROS_INFO("start tracking goal");
		cv::Point offset;
		cv::Point width(0,0);
		if(findGoalFunc(mat_trans, offset, width)) {
	        ROS_INFO("i found and go to");
			if(width.x >= 235) {
				ROS_INFO("width: %i", width.x);
		    	geometry_msgs::Twist vel;
			    vel.angular.z = 0;
			    vel.linear.x = 0;
			    vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
			    vel_pub_.publish(vel);
			}
			else {
        	double pan_scale_ = 0.001;
		    std_msgs::Float64 angle_msg;
		    angle_msg.data = pan_-pan_scale_*offset.x;
		    pan_pub_.publish(angle_msg);

			//go_count = 10;
		
		    //walking
		    double a_scale_ = 1;
		    geometry_msgs::Twist vel;
		    vel.angular.z = a_scale_*(angle_msg.data);
		    vel.linear.x = std::max(0.3,0.6- std::abs(5.0*vel.angular.z));//l_scale_*joy->axes[axis_linear_x_];
		    vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
		    vel_pub_.publish(vel);}
			}
		else {
			ROS_INFO("i try find");
			std_msgs::Float64 angle_msg;
       		angle_msg.data =  (sin(count_search_loop_*6.24/270));
        	pan_pub_.publish(angle_msg);
			count_search_loop_++;

			    geometry_msgs::Twist vel;
    vel.angular.z = 0;
    vel.linear.x = 0.4;
    vel.linear.y = -0.01;
    vel_pub_.publish(vel);
		}
	}
	
}



void RobotisOPBallTrackingNode::findGoal(cv::Mat& mat_trans){

	image_transport::ImageTransport it(nh_);
	image_transport::Publisher pub = it.advertise("/own/goal_detect/camera/image_raw", 40);
	sensor_msgs::ImagePtr msg2 = cv_bridge::CvImage(std_msgs::Header(), "mono8", mat_trans).toImageMsg();
	pub.publish(msg2);

	if(detected) {
		walkWithBall(mat_trans);
		
		return;
	}

	cv::Point offset;
	cv::Point width(0,0);

	if(direction == 0) {
		std_msgs::Float64 angle_msg;
	    angle_msg.data =  (sin(count_search_loop_*6.24/270));
    	pan_pub_.publish(angle_msg);
		count_search_loop_++;

		if(findGoalFunc(mat_trans, offset, width)) {
			if (pan_ >= 0) direction = 1;
			else direction = -1;

			count_search_loop_ = 0;
			angle_msg.data = 0;
			pan_pub_.publish(angle_msg);
			ros::Duration(1.5).sleep();
			
		}
		return;
	}
	ROS_INFO("direction %i", direction);

	
	bool goal_detected = findGoalFunc(mat_trans, offset, width);



	if(goal_detected){

		detected = true;


		ROS_INFO("offset: %d %d", offset.x, offset.y);

		//detected = true;



//        geometry_msgs::Twist vel;
//        vel.angular.z = -0.06;
//        vel.linear.x = 0;
//        vel.linear.y = 0;
//        vel_pub_.publish(vel);
//		ros::Duration(2).sleep();
//
		
//        vel.angular.z = 0;
//        vel.linear.x = 0;
//        vel.linear.y = 0;
//        vel_pub_.publish(vel);
//		ros::Duration(1).sleep();
  
	}
    else
    {

        geometry_msgs::Twist vel;
        vel.angular.z = 0.34*direction;
        vel.linear.x = 0;
        vel.linear.y = -0.15*direction;
        vel_pub_.publish(vel);

        count_search_loop_++;
    }
}
	

//http://wiki.ros.org/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
void RobotisOPBallTrackingNode::imageCb(const sensor_msgs::Image& msg)
{
    ROS_INFO("cb");

    //DETECT BALL
    cv_bridge::CvImagePtr image_trans, image_blob;
    image_trans = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::RGB8);
    image_blob = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::RGB8);

    cv::Mat& mat_blob = image_blob->image;
    cv::Mat& mat_trans = image_trans->image;
	cv::Mat test;
    cv::GaussianBlur( mat_trans, mat_trans, cv::Size(9, 9), 2, 2 );

	


	

	if(gone) {

		cv::inRange(mat_trans,cv::Scalar(180,180,180), cv::Scalar(255,255,255),mat_trans);
		image_transport::ImageTransport it(nh_);
		image_transport::Publisher pub = it.advertise("/own/goal_detect3/camera/image_raw", 1000);
		sensor_msgs::ImagePtr msg2 = cv_bridge::CvImage(std_msgs::Header(), "mono8", mat_trans).toImageMsg();
		pub.publish(msg2);
		findGoal(mat_trans);
		return;
	}
    std::vector<cv::Mat> canales;
    cv::split(mat_trans, canales);
    canales[0] =  2*canales[0]-canales[1]-canales[2];
    canales[1] = canales[2]-canales[2];
    canales[2] = canales[2]-canales[2];
    cv::merge(canales, mat_trans);


    cv::cvtColor(mat_trans,mat_trans, CV_RGB2GRAY);

    std::vector<cv::Vec3f> circles;

    ROS_INFO("%f %f %f %f %f %f",dp_, minDist_, param1_, param2_,min_radius_, max_radius_);

    cv::HoughCircles(mat_trans, circles, CV_HOUGH_GRADIENT, dp_, minDist_, param1_, param2_,min_radius_, max_radius_);//1,  mat_trans.rows/8, 200, 10, 0, 0 );




    ROS_INFO("detected %i circles",(int)circles.size());
    cv::Point ball_position;
    bool ball_detected = false;
    float min_dist;
    if(circles.size() > 0)
        min_dist = squaredDistXY(previous_position_, circles[0]);
    int min_idx = 0;
    for( size_t i = 0; i < circles.size(); i++ )
    {
        ball_detected = true;
        cv::Point center((circles[i][0]), (circles[i][1]));
        ball_position = center;
        int radius = (circles[i][2]);
        // circle center
        cv::circle( mat_blob, center, 3, cv::Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        cv::circle( mat_blob, center, radius, cv::Scalar(0,0,255), 3, 8, 0 );
        if(squaredDistXY(previous_position_, circles[0]) < min_dist)
        {
            min_idx = i;
            min_dist = squaredDistXY(previous_position_, circles[0]);
            ball_position = center;
        }
    }
    if(circles.size() > 0)
    {
        previous_position_ = circles[min_idx];
    }

    sensor_msgs::ImagePtr msg_out_trans;
    image_trans->encoding = "mono8";
    msg_out_trans = image_trans->toImageMsg();
    msg_out_trans->header.stamp = ros::Time::now();
    transformed_img_pub_.publish(*msg_out_trans);


    sensor_msgs::ImagePtr msg_out_blob;
    msg_out_blob = image_blob->toImageMsg();
    msg_out_blob->header.stamp = ros::Time::now();
    blob_img_pub_.publish(*msg_out_blob);

    if(ball_detected)
    {
        count_no_detection_ = 0;
        cv::Point image_center(mat_blob.size().width/2.0,mat_blob.size().height/2.0);
        cv::Point offset = ball_position - image_center ;
        ROS_INFO("ball pos  %i %i",ball_position.x, ball_position.y);
        ROS_INFO("ball pos offset %i %i",offset.x, offset.y);

	if (tilt_ >= -0.87) {

        	//head movement
	        double tilt_scale_ = 0.001;
        	double pan_scale_ = 0.001;
	        std_msgs::Float64 angle_msg;
	        angle_msg.data = tilt_-tilt_scale_*offset.y;
	        tilt_pub_.publish(angle_msg);
	        angle_msg.data = pan_-pan_scale_*offset.x;
	        pan_pub_.publish(angle_msg);
	
	        //walking
	        double a_scale_ = 0.5;
	        geometry_msgs::Twist vel;
	        vel.angular.z = a_scale_*(angle_msg.data);
	        vel.linear.x = std::max(0.0,0.8- std::abs(5.0*vel.angular.z));//l_scale_*joy->axes[axis_linear_x_];
	        vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
	        vel_pub_.publish(vel);
	}
	else {
		//stop
	        geometry_msgs::Twist vel;
	        vel.angular.z = 0;
	        vel.linear.x = 0;
	        vel.linear.y = 0;
	        vel_pub_.publish(vel);
			
			double tilt_scale_ = 0.0;
        	double pan_scale_ = 0.0;
	        std_msgs::Float64 angle_msg;
	        angle_msg.data = tilt_scale_;
	        tilt_pub_.publish(angle_msg);
	        angle_msg.data = pan_scale_;
	        pan_pub_.publish(angle_msg);
			gone = true;
	}
    }
    else if (count_no_detection_< 10)
    {
        count_no_detection_++;
        count_search_loop_ = 0;
    }
    else
    {
        //search loop
        std_msgs::Float64 angle_msg;
        angle_msg.data =  (sin(count_search_loop_*6.24/90))*0.5 - 0.5;
        tilt_pub_.publish(angle_msg);

        geometry_msgs::Twist vel;
        vel.angular.z = 0;
        vel.linear.x = 0;
        vel.linear.y = 0;
        vel_pub_.publish(vel);

        count_search_loop_++;
    }


}


}



int main(int argc, char **argv)
{

    ros::init(argc, argv, ROS_PACKAGE_NAME);

    ros::NodeHandle nh;
    double control_rate;
    nh.param("robotis_op_walking/control_rate", control_rate, 125.0);
    control_rate = 125.0;

    RobotisOPBallTrackingNode gazebo_walking_node(nh);

    ros::AsyncSpinner spinner(4);
    spinner.start();

    ros::Time last_time = ros::Time::now();
    ros::Rate rate(control_rate);



    dynamic_reconfigure::Server<robotis_op_ball_tracker::robotis_op_ball_trackerConfig> srv;
    dynamic_reconfigure::Server<robotis_op_ball_tracker::robotis_op_ball_trackerConfig>::CallbackType cb;
    cb = boost::bind(&RobotisOPBallTrackingNode::dynamicReconfigureCb, &gazebo_walking_node, _1, _2);
    srv.setCallback(cb);


    ROS_INFO("Ball tracking enabled");

    while (ros::ok())
    {
        rate.sleep();
        ros::Time current_time = ros::Time::now();
        ros::Duration elapsed_time = current_time - last_time;
        //gazebo_walking_node.Process();
        last_time = current_time;

    }

    return 0;
}

